import unittest
import sumrest

class TestSumrest(unittest.TestCase):

    def test_sum(self):

        result = sumrest.sum(10, 5)
        self.assertEqual(result, 15)

    def test_rest(self):

        result = sumrest.rest(8, 3)
        self.assertEqual(result, 5)
